class component_structure {
	key id: byte_array;
	name;
	type: component_type;
	extra: byte_array;
}

function createComponentStructure(name: text, type: component_type, extra: map<text, gtv>): component_structure {
	var extra_serialized: byte_array;	
	when(type) {
		component_type.INTEGER -> {
			require(extra.contains('int_values'));
			val intValues = require_not_empty(list<integer>.from_gtv(extra.get('int_values')), "Only integer values allowed");
			require(intValues.size() == 2, "Only min and max values are allowed");
			require(intValues[0] < intValues[1]);
			extra_serialized = extra_component_structure_integer(min_val = intValues[0], max_val = intValues[1]).to_bytes();
		};
		component_type.DECIMAL -> {
			require(extra.contains('int_values') and extra.contains('decimals'));
			val intValues = require_not_empty(list<integer>.from_gtv(extra.get('int_values')), "Only integer values allowed");
			val decimals = integer.from_gtv(extra.get('decimals'));
			require(intValues.size() == 2, "Only min and max values are allowed");
			require(intValues[0] < intValues[1]);
			extra_serialized = extra_component_structure_decimal(min_val = intValues[0], max_val = intValues[1], decimals).to_bytes();
		};
		component_type.ENUM -> {
			require(extra.contains('enum'));
			val allowed_values = list<text>.from_gtv(extra.get('enum'));
			require(allowed_values.size() < 256, "You can create maximum 256 enum items");
			val enums = set<text>();
			for (allowed_value in allowed_values) {
				enums.add(allowed_value.lower_case());
			}
			extra_serialized = extra_component_structure_enum(enums.sorted()).to_bytes();
		};
		// TODO: implement constraints for
		// 			- text (min max length)
		//			- byte_array (max_size)
		component_type.NFA -> {
			require(extra.contains('nfa_id'), "No nfa_id parameter in extra");
			val nfa_id = byte_array.from_gtv(extra.get('nfa_id'));
			val nfa = (nfa@{.id == nfa_id}(-sort .version, nfa_ref = nfa) limit 1).nfa_ref; // maybe extra param in "extra" and ask for the version to select?
			extra_serialized = extra_component_structure_nfa(nfa.id).to_bytes();
		};
		else -> {
			// type with no extra
			extra_serialized = x""; // not possible to create empty byte_array? 			
		};
	}
	
	val newComponentStructure = create component_structure (
		.name = name,
		.id = (name, chain_context.blockchain_rid).hash(),
		type,
		.extra = extra_serialized
	);
	
	
	return newComponentStructure;
}